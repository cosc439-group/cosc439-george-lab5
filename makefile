all: hellothread votecounter

hellothread: hellothread.c
	gcc -pthread -o hellothread hellothread.c

votecounter: votecounter.c
	gcc -pthread -o votecounter votecounter.c
