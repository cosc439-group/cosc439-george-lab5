#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *worker (void *args);

int main (){
  int n;
  printf ("please enter a number (1-10) of threads to create: ");
  scanf ("%d", &n);
	
  int idnum[n];
  pthread_t thread[n];

  for (int i = 1; i <= n; i++){
      idnum[i-1] = i;
      int tid = pthread_create (&thread[i-1], NULL, worker, &idnum[i-1]);
      
      printf ("Creating thread #id %d \n", i);
    }

  for (int j = 1; j <= n; j++){
      pthread_join (thread[j-1], NULL);
    }

}

void *worker (void *args){
  int t = *((int *) args);
  
  printf ("Hello world! I am thread %d \n", t);
  pthread_exit (NULL);
}
